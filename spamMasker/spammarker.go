package main

import (
	"fmt"
	"unicode"
)

func main()  {
	testOne := "Here's my spammy page: hTTp://youth-elixir.com"
	fmt.Println(spamMasker(testOne))

	testTwo := "Here's my spammy page: http://hehefouls.netHAHAHA see you."
	fmt.Println(spamMasker(testTwo))
}

func spamMasker(message string) string {
	messageBytes := []byte(message)
	maskedBytes := make([]byte, len(messageBytes))

	linkStart := []byte("http://")

	i := 0
	for i < (len(messageBytes) - len(linkStart)) {
		if bytesEqual(messageBytes[i:i+len(linkStart)], linkStart) {
			linkEndIndex := findLinkEndIndex(messageBytes[i:])
			if linkEndIndex > 0 {
				for j := 0; j < len(linkStart); j++ {
					maskedBytes[i+j] = linkStart[j]
				}
				for j := len(linkStart); j < linkEndIndex; j++ {
					maskedBytes[i+j] = '*'
				}
				i += linkEndIndex // Advance the index by the link's length
			} else {
				maskedBytes[i] = messageBytes[i]
				i++
			}
		} else {
			maskedBytes[i] = messageBytes[i]
			i++
		}
	}

	for i < len(messageBytes){
		maskedBytes[i] = messageBytes[i]
		i++
	}

	return string(maskedBytes)
}

func bytesEqual(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func findLinkEndIndex(messageBytes []byte) int {
	for i, b := range messageBytes {
		if unicode.IsSpace(rune(b)) || !unicode.IsPrint(rune(b)) {
			return i
		}
	}
	return len(messageBytes)
}

