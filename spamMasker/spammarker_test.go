package main

import (
	"testing"
)

func TestSpamMasker(t *testing.T) {
	testCases := []struct {
		input    string
		expected string
	}{
		{
			input:    "Here's my spammy page: hTTp://youth-elixir.com",
			expected: "Here's my spammy page: hTTp://youth-elixir.com",
		},
		{
			input:    "Here's my spammy page: http://hehefouls.netHAHAHA see you.",
			expected: "Here's my spammy page: http://******************* see you.",
		},
		{
			input:    "Here's my spammy page: http://hehefouls.netHAHAHA",
			expected: "Here's my spammy page: http://*******************",
		},
		{
			input:    "Here's my spammy page: http:////hehefouls.netHAHAHA",
			expected: "Here's my spammy page: http://*********************",
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.input, func(t *testing.T) {
			actual := spamMasker(testCase.input)
			if actual != testCase.expected {
				t.Errorf("Expected: %s, but got: %s", testCase.expected, actual)
			}
		})
	}
}

